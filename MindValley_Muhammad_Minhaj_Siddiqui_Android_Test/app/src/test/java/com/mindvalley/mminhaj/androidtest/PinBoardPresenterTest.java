package com.mindvalley.mminhaj.androidtest;

import com.mindvalley.mminhaj.androidtest.BasePinBoardTest;
import com.mindvalley.mminhaj.androidtest.model.PinBoard;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;


/**
 * Created by mani on 13/11/2016.
 */
@RunWith(PowerMockRunner.class)
public class PinBoardPresenterTest extends BasePinBoardTest {
    @Test
    public void testLoadPinBoardCalledWhenFetchingImagesFromService() {
        Observable<List<PinBoard>> emptyObservable = createEmptySerivceResponse();
        PowerMockito.when(mMockedPinBoardService.fetchPinBoardData())
                .thenReturn(emptyObservable);

        mMockedPresenter.fetchPinBoardImages();

        verify(mMockedPinBoardService, times(1)).fetchPinBoardData();
    }

    private Observable<List<PinBoard>> createEmptySerivceResponse() {
        List<PinBoard> pinBoardList = new ArrayList<>();
        return Observable.just(pinBoardList);
    }

}