package com.mindvalley.mminhaj.androidtest;

import com.mindvalley.mminhaj.androidtest.apis.PinBoardApi;
import com.mindvalley.mminhaj.androidtest.presenter.PinBoardPresenter;
import com.mindvalley.mminhaj.androidtest.services.PinBoardService;
import com.mindvalley.mminhaj.androidtest.view.IPinBoardView;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.List;

import rx.android.schedulers.AndroidSchedulers;
import rx.observers.TestSubscriber;
import rx.schedulers.Schedulers;

/**
 * Created by mani on 13/11/2016.
 */

@RunWith(PowerMockRunner.class)
public class BasePinBoardTest {

    @Mock
    protected PinBoardPresenter mMockedPresenter;

    @Mock
    protected PinBoardApi mMockedPinBoardApi;

    @Mock
    protected IPinBoardView mMockedPinBoardView;

    @Mock
    protected PinBoardService mMockedPinBoardService;

    @Before
    public void setup() throws Exception {
        MockitoAnnotations.initMocks(this);

        mMockedPresenter = new PinBoardPresenter(
                mMockedPinBoardView,
                mMockedPinBoardService);

        PowerMockito.stub(PowerMockito.method(AndroidSchedulers.class, "mainThread"))
                .toReturn(Schedulers.immediate());
    }

    protected <T> List<T> run(ObservableTester<T> runner, boolean assertOnErrors) {
        TestSubscriber<T> ts = new TestSubscriber<>();

        runner.run().subscribe(ts);
        ts.awaitTerminalEvent();

        if (assertOnErrors) {
            ts.assertNoErrors();
        }

        return ts.getOnNextEvents();
    }
}
