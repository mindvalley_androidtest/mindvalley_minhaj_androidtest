package com.mindvalley.mminhaj.androidtest;

import com.mindvalley.mminhaj.androidtest.services.PinBoardService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import androidtest.mminhaj.mindvalley.com.resourcemanager.ServiceGenerator;
import rx.Observable;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

/**
 * Created by mani on 13/11/2016.
 */

@RunWith(PowerMockRunner.class)
@PrepareForTest(ServiceGenerator.class)
public class PinBoardServiceTest extends BasePinBoardTest {

    @Mock
    ServiceGenerator serviceGenerator;

    @Test
    public void testLoadImages() throws  Exception{
        PowerMockito.when(mMockedPinBoardApi.loadPinBoard("123"))
                .thenReturn(Observable.empty());

        mockStatic(ServiceGenerator.class);
        PowerMockito.whenNew(PinBoardService.class)
                .withNoArguments().thenReturn(mMockedPinBoardService);

        PinBoardService pinBoardService = new PinBoardService();

        PowerMockito.when(pinBoardService.fetchPinBoardData())
                .thenReturn(Observable.empty());


        run(()-> pinBoardService.fetchPinBoardData(), true);

        verify(mMockedPinBoardApi, times(1)).loadPinBoard("123");
    }
}
