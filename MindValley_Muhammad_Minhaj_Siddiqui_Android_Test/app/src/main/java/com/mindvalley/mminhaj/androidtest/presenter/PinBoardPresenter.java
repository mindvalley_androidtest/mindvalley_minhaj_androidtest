package com.mindvalley.mminhaj.androidtest.presenter;

import android.util.Log;

import com.mindvalley.mminhaj.androidtest.services.PinBoardService;

import com.mindvalley.mminhaj.androidtest.model.PinBoard;
import com.mindvalley.mminhaj.androidtest.view.IPinBoardView;

import java.util.List;

import rx.Observer;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

import rx.schedulers.Schedulers;

/**
 * Created by mani on 12/11/2016.
 */

public class PinBoardPresenter  {

    private IPinBoardView mView;
    private PinBoardService mService;
    private Subscription mSubscription;

    public PinBoardPresenter(IPinBoardView view, PinBoardService service) {
        mView = view;
        mService = service;
    }

    public void removeReferences(){
        if(mSubscription != null && !mSubscription.isUnsubscribed()){
            mSubscription.unsubscribe();
        }
    }

    public void fetchPinBoardImages() {
        if(mSubscription != null && !mSubscription.isUnsubscribed()){
            mSubscription.unsubscribe();
        }

        mSubscription = mService.fetchPinBoardData()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<PinBoard>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("Error", e.getMessage());
                    }

                    @Override
                    public void onNext(List<PinBoard> pinBoard) {
                        compileImagesforPinBoard(pinBoard);
                    }
                });
    }

    private void compileImagesforPinBoard(List<PinBoard> pinBoard){
        mView.setPinBoardAdapter(pinBoard);
    }
}
