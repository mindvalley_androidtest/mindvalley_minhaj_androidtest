package com.mindvalley.mminhaj.androidtest.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mani on 12/11/2016.
 */
public class Categories {
    int id;

    String title;

    @SerializedName("photo_count")
    int photoCount;

    public Links links;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getPhotoCount() {
        return photoCount;
    }

    public void setPhotoCount(int photoCount) {
        this.photoCount = photoCount;
    }

    public Links getLinks() {
        return links;
    }

    public void setLinks(Links links) {
        this.links = links;
    }
}
