package com.mindvalley.mminhaj.androidtest.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;
import android.view.Window;

import com.mindvalley.mminhaj.androidtest.services.PinBoardService;
import com.mindvalley.mminhaj.androidtest.R;
import com.mindvalley.mminhaj.androidtest.adapter.PinBoardAdapter;
import com.mindvalley.mminhaj.androidtest.model.PinBoard;
import com.mindvalley.mminhaj.androidtest.presenter.PinBoardPresenter;
import com.mindvalley.mminhaj.androidtest.view.IPinBoardView;

import java.util.List;

/**
 * Created by mani on 12/11/2016.
 */

public class PinBoardActivity extends AppCompatActivity implements IPinBoardView {

    private StaggeredGridLayoutManager mStaggeredGridLayoutManager;
    private RecyclerView mRecyclerView;

    private PinBoardAdapter mPinBoardAdapter;

    private PinBoardPresenter mPinBoardPresenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pinboard);

        mPinBoardPresenter = new PinBoardPresenter(this, new PinBoardService());

        mRecyclerView = (RecyclerView)findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(false);

        mStaggeredGridLayoutManager = new StaggeredGridLayoutManager(2,
                StaggeredGridLayoutManager.VERTICAL);

        mRecyclerView.setLayoutManager(mStaggeredGridLayoutManager);

        onLoadScreen();
    }

    private void onLoadScreen() {
         mPinBoardPresenter.fetchPinBoardImages();
    }

    @Override
    public void setPinBoardAdapter(List<PinBoard> pinsToDownload) {
        mPinBoardAdapter = new PinBoardAdapter(this, pinsToDownload);
        mRecyclerView.setAdapter(mPinBoardAdapter);
    }

    @Override
    public void onPinSelected(View view) {
        //TODO: Implement onPinSelected method to open
        //      Pin in a new fragment or activity.
    }

    @Override
    public void onPinBoardDestroy() {
        mPinBoardPresenter.removeReferences();
    }
}