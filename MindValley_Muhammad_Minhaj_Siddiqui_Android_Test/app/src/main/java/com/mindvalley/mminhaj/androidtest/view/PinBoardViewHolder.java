package com.mindvalley.mminhaj.androidtest.view;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.mindvalley.mminhaj.androidtest.R;

/**
 * Created by mani on 13/11/2016.
 */

public class PinBoardViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView pinName;
    public ImageView pinImage;

    public PinBoardViewHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);
        pinName = (TextView) itemView.findViewById(R.id.pinName);
        pinImage = (ImageView) itemView.findViewById(R.id.pinImage);
    }

    @Override
    public void onClick(View view) {
        //TODO: Open a new activity with more info about this Pin.
    }
}