package com.mindvalley.mminhaj.androidtest.apis;

import retrofit2.http.Path;
import rx.Observable;

import com.mindvalley.mminhaj.androidtest.model.PinBoard;

import java.util.List;

import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by mani on 12/11/2016.
 */

public interface PinBoardApi {
    @GET("/raw/{pinBoardId}")
    Observable<List<PinBoard>> loadPinBoard(@Path("pinBoardId") String pinBoardId);
}
