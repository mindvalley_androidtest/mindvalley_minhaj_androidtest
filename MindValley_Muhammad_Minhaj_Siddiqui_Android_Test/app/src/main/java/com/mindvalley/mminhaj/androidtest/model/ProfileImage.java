package com.mindvalley.mminhaj.androidtest.model;

/**
 * Created by mani on 12/11/2016.
 */
public class ProfileImage {
    String small;
    String medium;
    String large;

    public String getSmall() {
        return small;
    }

    public void setSmall(String small) {
        this.small = small;
    }

    public String getMedium() {
        return medium;
    }

    public void setMedium(String medium) {
        this.medium = medium;
    }

    public String getLarge() {
        return large;
    }

    public void setLarge(String large) {
        this.large = large;
    }
}
