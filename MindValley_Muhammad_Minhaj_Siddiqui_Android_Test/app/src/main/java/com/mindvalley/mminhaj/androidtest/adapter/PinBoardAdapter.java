package com.mindvalley.mminhaj.androidtest.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mindvalley.mminhaj.androidtest.R;
import com.mindvalley.mminhaj.androidtest.model.PinBoard;

import com.mindvalley.mminhaj.androidtest.view.PinBoardViewHolder;

import java.util.List;

import androidtest.mminhaj.mindvalley.com.resourcemanager.controller.ImageDownloader;

/**
 * Created by mani on 12/11/2016.
 */

public class PinBoardAdapter extends RecyclerView.Adapter<PinBoardViewHolder> {

    private List<PinBoard> itemList;
    private Context context = null;
    ImageDownloader imageDownloader;

    public PinBoardAdapter(Context context, List<PinBoard> itemList) {
        this.itemList = itemList;
        this.context = context;

        imageDownloader = new ImageDownloader(context);
    }

    @Override
    public PinBoardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_pinboadlist, null);
        PinBoardViewHolder pinBoardRecyclerView = new PinBoardViewHolder(layoutView);
        return pinBoardRecyclerView;
    }

    @Override
    public void onBindViewHolder(PinBoardViewHolder holder, int position) {
        String url = itemList.get(position).getUrls().getFull();

        holder.pinName.setText(itemList.get(position).getUser().getName());

        imageDownloader.loadImage(url, holder.pinImage);
    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }
}
