package com.mindvalley.mminhaj.androidtest.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;

import com.mindvalley.mminhaj.androidtest.model.PinBoard;

import java.util.List;

/**
 * Created by mani on 12/11/2016.
 */

public interface IPinBoardView {
    void setPinBoardAdapter(List<PinBoard> pinsToDownload);
    void onPinSelected(View view);
    void onPinBoardDestroy();
}
