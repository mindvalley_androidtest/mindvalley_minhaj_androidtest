package com.mindvalley.mminhaj.androidtest.activities;

import android.content.Context;
import android.content.Intent;
import android.icu.util.TimeUnit;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;

import com.mindvalley.mminhaj.androidtest.R;

/**
 * Created by mani on 12/11/2016.
 */
public class SplashActivity extends AppCompatActivity {

    private Context mContext;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mContext = this;
        setContentView(R.layout.activity_splash);

        new Thread(()->{
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                startActivity(new Intent(mContext, PinBoardActivity.class));
                finish();
        }).start();

    }
}
