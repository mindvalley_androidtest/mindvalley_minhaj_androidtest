package com.mindvalley.mminhaj.androidtest.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by mani on 12/11/2016.
 */

public class PinBoard {
    String id;

    @SerializedName("created_at")
    String createdAt;

    long width;
    long height;
    String colorl;
    int likes;

    @SerializedName("liked_by_user")
    boolean likedByUser;

    @SerializedName("user")
    public User user;

    @SerializedName("urls")
    public Urls urls;

    @SerializedName("categories")
    public List<Categories> categories;

    @SerializedName("link")
    public Links link;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public long getWidth() {
        return width;
    }

    public void setWidth(long width) {
        this.width = width;
    }

    public long getHeight() {
        return height;
    }

    public void setHeight(long height) {
        this.height = height;
    }

    public String getColorl() {
        return colorl;
    }

    public void setColorl(String colorl) {
        this.colorl = colorl;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public boolean isLikedByUser() {
        return likedByUser;
    }

    public void setLikedByUser(boolean likedByUser) {
        this.likedByUser = likedByUser;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Urls getUrls() {
        return urls;
    }

    public void setUrls(Urls urls) {
        this.urls = urls;
    }

    public List<Categories> getCategories() {
        return categories;
    }

    public void setCategories(List<Categories> categories) {
        this.categories = categories;
    }

    public Links getLink() {
        return link;
    }

    public void setLink(Links link) {
        this.link = link;
    }
}
