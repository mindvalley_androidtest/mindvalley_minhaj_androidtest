package com.mindvalley.mminhaj.androidtest.services;

import rx.Observable;

import com.mindvalley.mminhaj.androidtest.apis.PinBoardApi;
import com.mindvalley.mminhaj.androidtest.model.PinBoard;

import java.util.List;

import androidtest.mminhaj.mindvalley.com.resourcemanager.ServiceGenerator;


/**
 * Created by mani on 12/11/2016.
 */

public class PinBoardService {

    String API_BASE_URL = "http://pastebin.com";

    private PinBoardApi mPinBoardApi;

    public PinBoardService(){
        mPinBoardApi = ServiceGenerator.
                getInstance()
                .createServiceforJson(PinBoardApi.class, API_BASE_URL);
    }

    public Observable<List<PinBoard>> fetchPinBoardData(){
        return mPinBoardApi.loadPinBoard("wgkJgazE");
    }
}
