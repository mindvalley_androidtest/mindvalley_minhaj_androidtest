package com.mindvalley.mminhaj.androidtest.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mani on 12/11/2016.
 */
public class User {
    String id;
    String userName;
    String name;

    @SerializedName("profile_image")
    public ProfileImage profileImage;

    public Links links;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ProfileImage getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(ProfileImage profileImage) {
        this.profileImage = profileImage;
    }

    public Links getLinks() {
        return links;
    }

    public void setLinks(Links links) {
        this.links = links;
    }
}
