package com.mindvalley.mminhaj.androidtest.model;

/**
 * Created by mani on 12/11/2016.
 */
public class Urls {
    String raw;
    String full;
    String regular;
    String smaller;
    String thumb;

    public String getRaw() {
        return raw;
    }

    public void setRaw(String raw) {
        this.raw = raw;
    }

    public String getFull() {
        return full;
    }

    public void setFull(String full) {
        this.full = full;
    }

    public String getRegular() {
        return regular;
    }

    public void setRegular(String regular) {
        this.regular = regular;
    }

    public String getSmaller() {
        return smaller;
    }

    public void setSmaller(String smaller) {
        this.smaller = smaller;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }
}
