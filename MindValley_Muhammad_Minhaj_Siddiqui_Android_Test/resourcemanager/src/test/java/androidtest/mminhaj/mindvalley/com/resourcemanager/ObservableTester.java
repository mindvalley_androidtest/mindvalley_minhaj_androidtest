package androidtest.mminhaj.mindvalley.com.resourcemanager;

import rx.Observable;

/**
 * Created by mani on 13/11/2016.
 */
public interface ObservableTester<T> {
    Observable<T> run();
}
