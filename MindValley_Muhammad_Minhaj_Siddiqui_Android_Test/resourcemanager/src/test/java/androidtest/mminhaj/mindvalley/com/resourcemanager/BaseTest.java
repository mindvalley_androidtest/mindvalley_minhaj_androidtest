package androidtest.mminhaj.mindvalley.com.resourcemanager;


import org.junit.Before;
import org.junit.runner.RunWith;

import org.mockito.MockitoAnnotations;

import org.powermock.modules.junit4.PowerMockRunner;

import java.util.List;
import rx.observers.TestSubscriber;


/**
 * Created by mani on 13/11/2016.
 */

@RunWith(PowerMockRunner.class)
public class BaseTest {


    @Before
    public void setup() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    protected <T> List<T> run(ObservableTester<T> runner, boolean assertOnErrors) {
        TestSubscriber<T> ts = new TestSubscriber<>();

        runner.run().subscribe(ts);
        ts.awaitTerminalEvent();

        if (assertOnErrors) {
            ts.assertNoErrors();
        }

        return ts.getOnNextEvents();
    }
}
