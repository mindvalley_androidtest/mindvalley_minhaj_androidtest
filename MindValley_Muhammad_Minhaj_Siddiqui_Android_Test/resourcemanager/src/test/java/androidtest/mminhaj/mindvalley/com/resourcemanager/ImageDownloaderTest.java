package androidtest.mminhaj.mindvalley.com.resourcemanager;

import android.content.Context;
import android.widget.ImageView;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.File;
import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;

import androidtest.mminhaj.mindvalley.com.resourcemanager.cache.FileCache;
import androidtest.mminhaj.mindvalley.com.resourcemanager.cache.MemoryCache;
import androidtest.mminhaj.mindvalley.com.resourcemanager.controller.ImageDownloader;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.whenNew;


/**
 * Created by mani on 14/11/2016.
 */


@RunWith(PowerMockRunner.class)
@PrepareForTest(Collections.class)
public class ImageDownloaderTest extends BaseTest{


    @Mock
    private Context mMockedContext;

    @Mock
    MemoryCache mMockedMemoryCache;

    @Mock
    FileCache mMockedFileCache;

    @Mock
    File mMockedFile;

    private ImageDownloader imageDownloader;

    @Mock
    Map<Object, Object> mMockedMappedImage;

    @Test
    public void testIfImageURLOrImageViewIsNull() throws Exception {
        PowerMockito.doNothing().when(mMockedMemoryCache).setLimit(anyLong());

        imageDownloader = new ImageDownloader(mMockedContext);

        imageDownloader.loadImage(null, null);

        verify(mMockedMappedImage, times(0)).put(anyObject(), anyString());
    }

    @Test
    public void testHashMapforImageUrlIsCalled() throws Exception {
        ImageView mMockedImageView = mock(ImageView.class);

        mock(WeakHashMap.class);

        PowerMockito.mockStatic(Collections.class);

        PowerMockito.doNothing().when(mMockedMemoryCache).setLimit(anyLong());

        imageDownloader = new ImageDownloader(mMockedContext);

        imageDownloader.loadImage("123", mMockedImageView);

        verify(mMockedMappedImage, times(1)).put(anyObject(), anyString());
    }

}