package androidtest.mminhaj.mindvalley.com.resourcemanager;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by mani on 07/11/2016.
 */

public class ServiceGenerator {

    private static ServiceGenerator mServiceGenerator;

    private static OkHttpClient.Builder mHttpClient;

    private static HttpLoggingInterceptor logging ;

    public static ServiceGenerator getInstance(){
        if(mServiceGenerator == null){

            mHttpClient = new OkHttpClient.Builder();
            logging = new HttpLoggingInterceptor();

            mServiceGenerator = new ServiceGenerator();
        }

        return mServiceGenerator;
    }

    private Retrofit.Builder buildRetrofitObj(String baseAPI_URL){
        // set your desired log level
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        mHttpClient.addInterceptor(logging);

        return new Retrofit.Builder()
                .baseUrl(baseAPI_URL)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create());
    }

    public <S> S createServiceforJson(Class<S> serviceClass, String endPoint){
        return buildRetrofitObj(endPoint)
                .addConverterFactory(GsonConverterFactory.create())
                .client(mHttpClient.build())
                .build()
                .create(serviceClass);

    }

//TODO: Uncomment this method to get Service in XML
//    public <S> S createServiceforXML(Class<S> serviceClass, String endPoint){
//        return buildRetrofitObj(endPoint)
//                .addConverterFactory(SimpleXmlConverterFactory.create())
//                .client(mHttpClient.build())
//                .build()
//                .create(serviceClass);
//    }

}
