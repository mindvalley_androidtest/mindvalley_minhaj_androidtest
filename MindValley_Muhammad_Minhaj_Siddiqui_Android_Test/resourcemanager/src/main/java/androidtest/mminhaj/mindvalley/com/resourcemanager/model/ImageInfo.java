package androidtest.mminhaj.mindvalley.com.resourcemanager.model;

import android.widget.ImageView;

/**
 * Created by mani on 13/11/2016.
 */

public class ImageInfo {
    public String url;
    public ImageView imageView;

    public ImageInfo(String url, ImageView imageView){
        this.url = url;
        this.imageView = imageView;
    }
}
