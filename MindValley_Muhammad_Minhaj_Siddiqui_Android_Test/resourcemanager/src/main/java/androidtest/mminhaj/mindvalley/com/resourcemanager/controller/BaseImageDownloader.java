package androidtest.mminhaj.mindvalley.com.resourcemanager.controller;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by mani on 13/11/2016.
 */
public abstract class BaseImageDownloader {
    abstract protected InputStream getInputStream(String url) throws IOException;
}
