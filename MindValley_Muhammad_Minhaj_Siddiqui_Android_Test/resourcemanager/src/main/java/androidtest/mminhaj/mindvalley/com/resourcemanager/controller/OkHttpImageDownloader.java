package androidtest.mminhaj.mindvalley.com.resourcemanager.controller;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

/**
 * Created by mani on 12/11/2016.
 */

public class OkHttpImageDownloader extends BaseImageDownloader
{
    private static OkHttpImageDownloader mOkHttpImageDownloader;

    private static OkHttpClient mClient;
    private static final int MIN_DISK_CACHE_SIZE = 10 * 1024 * 1024; // 10MB

    public static OkHttpImageDownloader getInstance(File cacheFile){
        if(mOkHttpImageDownloader == null){
            mClient = createOkHttpClient(cacheFile, MIN_DISK_CACHE_SIZE);
            mOkHttpImageDownloader = new OkHttpImageDownloader();
        }

        return mOkHttpImageDownloader;
    }

    private static OkHttpClient createOkHttpClient(File cacheDir, int discCacheSize) {
        return new OkHttpClient.Builder()
                .cache(new Cache(cacheDir, discCacheSize))
                .connectTimeout(5, TimeUnit.SECONDS)
                .readTimeout(20, TimeUnit.SECONDS)
                .retryOnConnectionFailure(true)
                .build();
    }

    @Override
    protected InputStream getInputStream(String imageUri) throws IOException {
        Request request = new Request.Builder().url(imageUri).build();
        Response response = mClient.newCall(request).execute();

        int responseCode = response.code();
        if (responseCode >= 300) {
            response.body().close();
            return null;
        }

        ResponseBody responseBody = mClient.newCall(request).execute().body();

        InputStream inputStream = responseBody.byteStream();
        return inputStream;
    }
}
