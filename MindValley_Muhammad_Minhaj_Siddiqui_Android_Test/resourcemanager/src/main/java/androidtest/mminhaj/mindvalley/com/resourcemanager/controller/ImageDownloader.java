package androidtest.mminhaj.mindvalley.com.resourcemanager.controller;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.widget.ImageView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


import androidtest.mminhaj.mindvalley.com.resourcemanager.Helper.Utils;

import androidtest.mminhaj.mindvalley.com.resourcemanager.R;
import androidtest.mminhaj.mindvalley.com.resourcemanager.cache.FileCache;
import androidtest.mminhaj.mindvalley.com.resourcemanager.cache.MemoryCache;
import androidtest.mminhaj.mindvalley.com.resourcemanager.model.ImageInfo;

/**
 * Created by mani on 12/11/2016.
 */

public class ImageDownloader {

    MemoryCache mMemoryCache = new MemoryCache();
    FileCache mFileCache;
    Map<ImageView, String> mMapedImageViews;

    ExecutorService mExecutorService;
    Handler mHandler = new Handler();//mHandler to display images in UI thread

    public ImageDownloader(Context context){
        setMapImageViews();

        mFileCache = new FileCache(context);
        mExecutorService = Executors.newFixedThreadPool(5);
    }

    final int stub_id = R.drawable.stub;

    public void loadImage(String url, ImageView imageView)
    {
        if(url == null || imageView == null){
            return;
        }

        if(url.isEmpty()){
            return;
        }

        mMapedImageViews.put(imageView, url);

        Bitmap bitmap = mMemoryCache.get(url);
        if(bitmap!=null)
            imageView.setImageBitmap(bitmap);
        else
        {
            queuePhoto(url, imageView);
            imageView.setImageResource(stub_id);
        }
    }

    private void queuePhoto(String url, ImageView imageView) {
        ImageInfo p=new ImageInfo(url, imageView);
        mExecutorService.submit(new ImageLoader(p));
    }

    private Bitmap getBitmap(String url) {
        File file = mFileCache.getFile(url);

        // check if bitmap is available in cache.
        Bitmap bmp = decodeFile(file);
        if(bmp != null)
            return bmp;

        // otherwise download from server.
        try {
            Bitmap bitmap;
            InputStream is = OkHttpImageDownloader
                    .getInstance(mFileCache.getFile("Http_Cache"))
                    .getInputStream(url);
            OutputStream os = new FileOutputStream(file);
            Utils.CopyStream(is, os);
            os.close();
            //conn.disconnect();
            bitmap = decodeFile(file);
            return bitmap;
        } catch (Throwable ex){
            ex.printStackTrace();
            if(ex instanceof OutOfMemoryError)
                mMemoryCache.clear();
            return null;
        }
    }

    //decodes image and scales it to reduce memory consumption
    private Bitmap decodeFile(File f){
        try {
            //decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            FileInputStream stream1=new FileInputStream(f);
            BitmapFactory.decodeStream(stream1,null,o);
            stream1.close();

            //Find the correct scale value. It should be the power of 2.
            final int REQUIRED_SIZE = 70;
            int width_tmp = o.outWidth, height_tmp = o.outHeight;
            int scale = 1;
            while(true){
                if(width_tmp/2 < REQUIRED_SIZE || height_tmp/2 < REQUIRED_SIZE)
                    break;
                width_tmp /= 2;
                height_tmp /= 2;
                scale *= 2;
            }

            //decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize=scale;
            FileInputStream stream2 = new FileInputStream(f);
            Bitmap bitmap=BitmapFactory.decodeStream(stream2, null, o2);
            stream2.close();
            return bitmap;
        } catch (FileNotFoundException e) {
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    class ImageLoader implements Runnable {
        ImageInfo photoToLoad;

        ImageLoader(ImageInfo photoToLoad){
            this.photoToLoad=photoToLoad;
        }

        @Override
        public void run() {
            try{
                if(imageViewReused(photoToLoad))
                    return;
                Bitmap bmp=getBitmap(photoToLoad.url);
                mMemoryCache.put(photoToLoad.url, bmp);
                if(imageViewReused(photoToLoad))
                    return;
                BitmapDisplayer bd=new BitmapDisplayer(bmp, photoToLoad);
                mHandler.post(bd);
            }catch(Throwable th){
                th.printStackTrace();
            }
        }
    }

    boolean imageViewReused(ImageInfo photoToLoad){
        String tag= mMapedImageViews.get(photoToLoad.imageView);
        if(tag==null || !tag.equals(photoToLoad.url))
            return true;
        return false;
    }

    //Used to display bitmap in the UI thread
    class BitmapDisplayer implements Runnable
    {
        Bitmap bitmap;
        ImageInfo photoToLoad;
        public BitmapDisplayer(Bitmap b, ImageInfo p) {
            bitmap = b;
            photoToLoad=p;
        }

        public void run() {
            if(imageViewReused(photoToLoad))
                return;
            if(bitmap!=null)
                photoToLoad.imageView.setImageBitmap(bitmap);
            else
                photoToLoad.imageView.setImageResource(stub_id);
        }
    }

    public Map<ImageView, String> getmMapedImageViews() {
        return mMapedImageViews;
    }

    public void setMapImageViews() {
        this.mMapedImageViews =
                Collections.synchronizedMap(new WeakHashMap<ImageView, String>());
    }

    public void clearCache() {
        mMemoryCache.clear();
        mFileCache.clear();
    }

}
